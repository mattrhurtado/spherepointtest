# README #

### What is this repository for? ###

* Proof of algorithm to detect whether a point (in this case, the center of the "OutsideSphere" object) is within a sphere with a given center point and radius
* Version 1

### How do I get set up? ###

* Install Unity version 2017.1.0f3
* Clone this repo
* Open Assets/spherepointtest.unity

### Notes ###

* Since this is a simple proof of concept, the "OutsideSphere" object will need to be moved within the Unity interface before the game is started
* When starting the game, if the "OutsideSphere"'s center point is within the "InsideSphere", then it will log "True" to the console, otherwise it will log "False"