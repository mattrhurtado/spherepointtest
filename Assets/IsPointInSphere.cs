﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsPointInSphere : MonoBehaviour {

    private SphereCollider sphereCollider2;
    private GameObject sphere1;
    private GameObject sphere2;
	// Use this for initialization
	void Start () {
        sphere1 = GameObject.Find("OutsideSphere");
        sphere2 = GameObject.Find("InsideSphere");        
        sphereCollider2 = sphere2.GetComponent<SphereCollider>();
        Debug.Log(sphereCollider2.radius*2);
        Debug.Log(DoesPointExistInSphere(sphere1.transform.position.x, sphere1.transform.position.y, sphere1.transform.position.z, sphere2.transform.position.x, sphere2.transform.position.y, sphere2.transform.position.z, sphereCollider2.radius * 2));

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    static bool DoesPointExistInSphere(float x1, float y1, float z1, float x2, float y2, float z2, float r)
    {
        double pointExist = Mathf.Pow((x2 - x1), 2) + Mathf.Pow((y2 - y1), 2) + Mathf.Pow((z2 - z1), 2) / Mathf.Pow(r, 2);
        if (pointExist <= Mathf.Pow(r, 2))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
